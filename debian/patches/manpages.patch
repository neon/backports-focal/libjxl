Description: Clean up man pages
Author: Mathieu Malaterre <malat@debian.org>
Forwarded: https://github.com/libjxl/libjxl/pull/1288
Last-Update: 2022-03-29

Index: libjxl/doc/man/cjxl.txt
===================================================================
--- libjxl.orig/doc/man/cjxl.txt
+++ libjxl/doc/man/cjxl.txt
@@ -31,16 +31,14 @@ cjxl input.gif output.jxl
 Options
 -------
 
--h::
---help::
-    Displays the options that `cjxl` supports. On its own, it will only show
-    basic options. It can be combined with `-v` or `-v -v` to show increasingly
-    advanced options as well.
+--container::
+    Always encode using container format (default: only if needed)
 
--v::
---verbose::
-    Increases verbosity. Can be repeated to increase it further, and also
-    applies to `--help`.
+--strip::
+    Do not encode using container format (strips Exif/XMP/JPEG bitstream reconstruction data)
+
+--strip_jpeg_metadata::
+    Do not encode JPEG bitstream reconstruction data
 
 -d 'distance'::
 --distance='distance'::
@@ -51,6 +49,18 @@ Options
     GIF files are compressed losslessly by default, and in the case of JPEG
     files specifically, the original JPEG can then be reconstructed bit-for-bit.
     For lossless sources, `-d 1` is the default.
+    0.0 = mathematically lossless. Default for already-lossy input (JPEG/GIF).
+    1.0 = visually lossless. Default for other input.
+    Recommended range: 0.5 .. 3.0.
+
+--target_size='N'::
+    Aim at file size of N bytes.
+    Compresses to 1 % of the target size in ideal conditions.
+    Runs the same algorithm as --target_bpp
+
+--target_bpp='BPP'::
+    Aim at file size that has N bits per pixel.
+    Compresses to 1 % of the target BPP in ideal conditions.
 
 -q 'quality'::
 --quality='quality'::
@@ -78,6 +88,208 @@ Recognized effort settings, from fastest
 - 8 or ``kitten''
 - 9 or ``tortoise''
 
+--brotli_effort='B_EFFORT'::
+    Brotli effort setting. Range: 0 .. 11.
+    Default: -1 (based on EFFORT). Higher number is more effort (slower).
+
+-s 'ANIMAL'::
+--speed='ANIMAL'::
+    Deprecated synonym for --effort. Valid values are:
+    lightning (1), thunder, falcon, cheetah, hare, wombat, squirrel, kitten, tortoise (9)
+    Default: squirrel. Values are in order from faster to slower.
+
+--faster_decoding='AMOUNT'::
+    Favour higher decoding speed. 0 = default, higher values give higher speed at the expense of quality
+
+-p::
+--progressive::
+    Enable progressive/responsive decoding.
+
+--premultiply::
+    Force premultiplied (associated) alpha.
+
+--keep_invisible='0|1'::
+    Force disable/enable preserving color of invisible pixels (default: 1 if
+    lossless, 0 if lossy).
+
+--centerfirst::
+    Put center groups first in the compressed file.
+
+--center_x=0..XSIZE::
+    Put center groups first in the compressed file.
+
+--center_y=0..YSIZE::
+    Put center groups first in the compressed file.
+
+--progressive_ac::
+    Use the progressive mode for AC.
+
+--qprogressive_ac::
+    Use the progressive mode for AC.
+
+--progressive_dc='num_dc_frames'::
+    Use progressive mode for DC.
+
+-m::
+--modular::
+    Use the modular mode (lossy / lossless).
+
+--use_new_heuristics::
+    Use new and not yet ready encoder heuristics
+
+-j::
+--jpeg_transcode::
+    Do lossy transcode of input JPEG file (decode to pixels instead of doing
+    lossless transcode).
+
+--jpeg_transcode_disable_cfl::
+    Disable CFL for lossless JPEG recompression
+
+--num_threads='N'::
+    number of worker threads (zero = none).
+
+--num_reps='N'::
+    how many times to compress.
+
+--noise='0|1'::
+    force disable/enable noise generation.
+
+--photon_noise='ISO3200'::
+    Set the noise to approximately what it would be at a given nominal exposure
+    on a 35mm camera. For formats other than 35mm, or when the whole sensor was
+    not used, you can multiply the ISO value by the equivalence ratio squared,
+    for example by 2.25 for an APS-C camera.
+
+--dots='0|1'::
+    force disable/enable dots generation.
+
+--patches='0|1'::
+    force disable/enable patches generation.
+
+--resampling='-1|1|2|4|8'::
+    Subsample all color channels by this factor, or use -1 to choose the resampling factor based on distance.
+
+--ec_resampling='1|2|4|8'::
+    Subsample all extra channels by this factor. If this value is smaller than the resampling of color channels, it will be increased to match.
+
+--already_downsampled::
+    Do not downsample the given input before encoding, but still signal that the decoder should upsample.
+
+--epf='-1..3'::
+    Edge preserving filter level (-1 = choose based on quality, default)
+
+--gaborish='0|1'::
+    force disable/enable gaborish.
+
+--intensity_target='N'::
+    Intensity target of monitor in nits, higher results in higher quality
+    image. Must be strictly positive. Default is 255 for standard images, 4000
+    for input images known to to have PQ or HLG transfer function.
+
+--saliency_num_progressive_steps='N'::
+
+--saliency_map_filename='STRING'::
+
+--saliency_threshold='0..1'::
+
+-x 'key=value'::
+--dec-hints='key=value'::
+    color_space indicates the ColorEncoding, see Description(); cc_pathname
+    refers to a binary file containing an ICC profile.
+
+--override_bitdepth='0..32'::
+    0=use from image, 1-32=override.
+    If nonzero, store the given bit depth in the JPEG XL file metadata (1-32), instead of using the bit depth from the original input image.
+
+-c '0..2'::
+--colortransform='0..2'::
+    0=XYB, 1=None, 2=YCbCr
+
+-I 'F'::
+--iterations='F'::
+    [modular encoding] fraction of pixels used to learn MA trees (default=0.5, try 0 for no MA and fast decode)
+
+-C 'K'::
+--colorspace='K'::
+    [modular encoding] color transform: 0=RGB, 1=YCoCg, 2-37=RCT (default: try
+    several, depending on speed)
+
+-g 'K'::
+--group-size='K'::
+    [modular encoding] set group size to 128 << K (default: 1 or 2)
+
+-P 'K'::
+--predictor='K'::
+    [modular encoding] predictor(s) to use:
++
+- 0=zero,
+- 1=left,
+- 2=top,
+- 3=avg0,
+- 4=select,
+- 5=gradient,
+- 6=weighted,
+- 7=topright,
+- 8=topleft,
+- 9=leftleft,
+- 10=avg1,
+- 11=avg2,
+- 12=avg3,
+- 13=toptop predictive average
+- 14=mix 5 and 6,
+- 15=mix everything.
++
+Default 14, at slowest speed default 15
+
+-E 'K'::
+--extra-properties='K'::
+    [modular encoding] number of extra MA tree properties to use
+
+--palette='K'::
+    [modular encoding] use a palette if image has at most K colors (default:
+    1024)
+
+--lossy-palette::
+    [modular encoding] quantize to a palette that has fewer entries than would
+    be necessary for perfect preservation; for the time being, it is
+    recommended to set --palette=0 with this option to use the default palette only
+
+-X 'PERCENT'::
+--pre-compact='PERCENT'::
+    [modular encoding] compact channels (globally) if ratio used/range is below this (default: 80%)
+
+-Y 'PERCENT'::
+--post-compact='PERCENT'::
+    [modular encoding] compact channels (per-group) if ratio used/range is below this (default: 80%)
+
+-R 'K'::
+--responsive='K'::
+    [modular encoding] do Squeeze transform, 0=false, 1=true (default: true if
+    lossy, false if lossless)
+
+-V
+--version::
+    Print version number and exit
+
+--quiet::
+    Be more silent
+
+--print_profile='0|1'::
+    Print timing information before exiting
+
+-v::
+--verbose::
+    Increases verbosity. Can be repeated to increase it further, and also
+    applies to `--help`.
+
+-h::
+--help::
+    Displays the options that `cjxl` supports. On its own, it will only show
+    basic options. It can be combined with `-v` or `-v -v` to show increasingly
+    advanced options as well.
+
+
+
 Examples
 --------
 
Index: libjxl/doc/man/djxl.txt
===================================================================
--- libjxl.orig/doc/man/djxl.txt
+++ libjxl/doc/man/djxl.txt
@@ -24,9 +24,43 @@ produced, with names of the form "'outpu
 Options
 -------
 
--h::
---help::
-    Displays the options that `djxl` supports.
+-V::
+--version::
+    Print version number and exit
+
+--num_reps='N'::
+    How many times to decompress.
+
+--num_threads='N'::
+    The number of threads to use
+
+--print_profile='0|1'::
+    Print timing information before exiting
+
+--bits_per_sample='N'::
+    Defaults to original (input) bit depth
+
+--tone_map::
+    Tone map the image to the luminance range indicated by --display_nits instead of performing a naive 0-1 -> 0-1 conversion
+
+--display_nits='0.3-250'::
+    Luminance range of the display to which to tone-map; the lower bound can be omitted
+
+--preserve_saturation='0..1'::
+    With --tone_map, how much to favor saturation over luminance
+
+--color_space='RGB_D65_SRG_Rel_Lin'::
+    Defaults to original (input) color space
+
+-s '1,2,4,8,16'::
+--downsampling='1,2,4,8,16'::
+    Maximum permissible downsampling factor (values greater than 16 will return the LQIP if available)
+
+--allow_partial_files::
+    Allow decoding of truncated files
+
+--allow_more_progressive_steps::
+    Allow decoding more progressive steps in truncated files. No effect without --allow_partial_files
 
 -j::
 --pixels_to_jpeg::
@@ -36,12 +70,23 @@ Options
     This flag causes the decoder to instead decode the image to pixels and
     encode a new (lossy) JPEG in this case.
 
-
 -q 'quality'::
 --jpeg_quality='quality'::
     When decoding to `.jpg`, use this output quality. This option implicitly
     enables the --pixels_to_jpeg option.
 
+--print_read_bytes::
+   print total number of decoded bytes
+
+--quiet::
+   silence output (except for errors)
+
+-h::
+--help::
+    Displays the options that `djxl` supports.
+
+
+
 
 Examples
 --------
@@ -54,6 +99,25 @@ $ djxl input.jxl output.png
 $ djxl lossless-jpeg.jxl reconstructed.jpeg
 ----
 
+# Lossless compression
+
+Lossless pixel compression only preserves the pixels losslessly, not the input
+bitstream. To check that the pixels are identical, one can do something like
+the following (if this says 0, then the maximum pixel error is 0, so it's
+lossless):
+
+----
+# Lossless compression of PNG:
+$ cjxl -d 0.0 input.png lossless.png
+
+# Decompress a JPEG XL file to PNG
+$ djxl lossless.jxl lossless.png
+
+$ compare -metric pae input.png lossless.png null:
+0 (0)
+----
+
+
 
 See also
 --------
Index: libjxl/tools/djxl.cc
===================================================================
--- libjxl.orig/tools/djxl.cc
+++ libjxl/tools/djxl.cc
@@ -65,7 +65,8 @@ void DecompressArgs::AddCommandLineOptio
   cmdline->AddOptionFlag('V', "version", "print version number and exit",
                          &version, &SetBooleanTrue);
 
-  cmdline->AddOptionValue('\0', "num_reps", "N", nullptr, &num_reps,
+  cmdline->AddOptionValue('\0', "num_reps", "N",
+                          "how many times to decompress.", &num_reps,
                           &ParseUnsigned);
 
   cmdline->AddOptionValue('\0', "num_threads", "N",
