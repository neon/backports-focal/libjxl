jpeg-xl (0.7.0~git20220325.7594374+ds-3) experimental; urgency=medium

  * d/patches: Improve man pages documentation
  * d/u/metadata: Fix lintian upstream-metadata-file-is-missing
  * d/rules: Add missing jxlinfo generation rule
  * d/rules: Add missing rule for *_ng man pages
  * d/rules: Add flags from upstream during compilation
  * d/rules: Add benchmark command line to devtools
  * d/rules: Properly set doxygen to OFF when building binary only
  * d/patches: Properly install jar/jni files
  * d/control: Add libjpegxl-java package
  * d/rules: Fixed typo in DEB_VERSION_UPSTREAM

 -- Mathieu Malaterre <malat@debian.org>  Mon, 04 Apr 2022 11:08:50 +0200

jpeg-xl (0.7.0~git20220325.7594374+ds-2) experimental; urgency=medium

  * d/control: Add graphviz package for dot command
  * d/control: Add missing help2man dependency
  * d/rules: Only run help2man in arch builds
  * d/rules: Add missing rules to cleanup *.1 generated files

 -- Mathieu Malaterre <malat@debian.org>  Tue, 29 Mar 2022 09:06:43 +0200

jpeg-xl (0.7.0~git20220325.7594374+ds-1) experimental; urgency=medium

  * New upstream version 0.7.0~git20220325.7594374+ds
  * d/control: Document relation cjpeg_hdr to avifenc
  * d/manpages: Add missing devtools manpages
  * d/rules: Remove generated *.a static libs
  * d/manpages: Add tools manpages
  * d/rules: Add missing PHONY target
  * d/rules: Install jxlinfo in devtools
  * d/control: Add Suggests to netpbm
  * d/control: Add Recommends to libjpeg-progs
  * d/patches: Refresh patches
  * d/patches: Make sure to use lcms 2.12 for now
  * d/rules: Remove reference to dead code (epf_main)

 -- Mathieu Malaterre <malat@debian.org>  Mon, 28 Mar 2022 13:59:09 +0200

jpeg-xl (0.7.0~git20220228.89875cb+ds-1) experimental; urgency=medium

  * d/patches: Fix minor typo
  * New upstream version 0.7.0~git20220228.89875cb+ds
  * d/patches: Remove GUN/Hurd patch applied upstream
  * d/control: Add devtools package

 -- Mathieu Malaterre <malat@debian.org>  Thu, 03 Mar 2022 16:34:28 +0100

jpeg-xl (0.7.0~git20220120.0647da4+ds-5) experimental; urgency=medium

  * d/rules: Remove a warning note from all buildds
  * d/rules: Do not run test in indep builds
  * d/patches: Add thread fix for riscv64
  * d/patches: Rework riscv64 patch

 -- Mathieu Malaterre <malat@debian.org>  Mon, 07 Feb 2022 14:02:30 +0100

jpeg-xl (0.7.0~git20220120.0647da4+ds-4) experimental; urgency=medium

  * d/examples: Prefer doc package now
  * d/patches: Fix incomplete HURD patch
  * d/patches: Really fix riscv64 support

 -- Mathieu Malaterre <malat@debian.org>  Mon, 31 Jan 2022 13:16:53 +0100

jpeg-xl (0.7.0~git20220120.0647da4+ds-3) experimental; urgency=medium

  * d/control: Remove xdg-utils/xmlto dependencies (not needed)
  * d/control: We do not need asciidoc/dblatex dependency
  * d/control: Add jpeg-xl-doc package
  * d/control: Update requirement for cmake version
  * d/control: Add missing dependency docbook-xml
  * d/patches: Fix compilation on GNU/Hurd
  * d/patches: Add support for riscv64/atomics

 -- Mathieu Malaterre <malat@debian.org>  Fri, 28 Jan 2022 11:37:42 +0100

jpeg-xl (0.7.0~git20220120.0647da4+ds-2) experimental; urgency=medium

  * d/control: Make sure to specify minimum requirement for dependencies
  * d/symbols: Add missing Build-Depends-Package line
  * d/rules: Remove a warning on armel/armhf
  * d/patches: Start adding support for latomic arches
  * d/rules: Do not force tcmalloc implementation on all arches

 -- Mathieu Malaterre <malat@debian.org>  Thu, 27 Jan 2022 13:47:42 +0100

jpeg-xl (0.7.0~git20220120.0647da4+ds-1) experimental; urgency=medium

  * d/watch Prepare orig tarball directly from git HEAD
  * d/copyright Upstream has simplified convenient library copy logic
  * New upstream version 0.7.0~git20220120.0647da4+ds
  * d/control Start building jpeg-xl 0.7
  * d/symbols Update symbols file
  * d/patches Remove patches applied upstream
  * d/compat Start using compat 11
  * d/rules Document why plugins are not built currently
  * d/rules Start building JPEG-XL with OpenEXR option

 -- Mathieu Malaterre <malat@debian.org>  Thu, 20 Jan 2022 14:35:45 +0100

jpeg-xl (0.6.1+ds-6) experimental; urgency=medium

  * d/symbols: Fix lintian symbols-file-missing-build-depends-package-field
  * d/rules: Start using the system installed hwy
  * d/patches: Adapt to GCC-11 default c++ standard
  * d/examples: Install examples folder
  * d/install: Do not install *.a anymore

 -- Mathieu Malaterre <malat@debian.org>  Thu, 06 Jan 2022 15:39:42 +0100

jpeg-xl (0.6.1+ds-5) experimental; urgency=medium

  * d/symbols: Hide all the std namespace symbols
  * d/rules: Add missing -latomic for armel and such
  * d/patches: Do not force -std=c++11 flag during compilation

 -- Mathieu Malaterre <malat@debian.org>  Wed, 15 Dec 2021 16:19:00 +0100

jpeg-xl (0.6.1+ds-4) experimental; urgency=medium

  * d/patches: Properly handle a2x >= 10
  * d/rules: Do not check the return value from test

 -- Mathieu Malaterre <malat@debian.org>  Wed, 15 Dec 2021 12:07:35 +0100

jpeg-xl (0.6.1+ds-3) experimental; urgency=medium

  * d/control: Add missing B-D on LCMS
  * d/control: Add missing Rules-Requires-Root: no

 -- Mathieu Malaterre <malat@debian.org>  Wed, 15 Dec 2021 10:20:11 +0100

jpeg-xl (0.6.1+ds-2) experimental; urgency=medium

  * d/control: Add missing B-D on ninja
  * d/TODO: Add documentation about C-ABI and libstdc++ exceptions
  * d/README.Debian: Add documentation about git submodules

 -- Mathieu Malaterre <malat@debian.org>  Wed, 15 Dec 2021 09:13:47 +0100

jpeg-xl (0.6.1+ds-1) experimental; urgency=medium

  * Initial release (Closes: #948862)

 -- Mathieu Malaterre <malat@debian.org>  Tue, 14 Dec 2021 10:29:22 +0100
